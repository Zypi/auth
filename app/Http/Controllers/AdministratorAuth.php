<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use Hash;
use App\Models\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;

class AdministratorAuth extends Controller
{
    public function showpagelogin()    {
        return view ('adminauth.login');
    }

    public function postLoginAdmin(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        $credential = $request->only('email','password');
        if (Auth::attempt($credential)) {
            return redirect()->intended('dashboard')->withSuccess('Sign In');
        }
        return redirect()->route('administrator.login')->with('message', 'Login Gagal');
    }
    public function showpageregister(){
        return view('adminauth.adminregister');
    }
    public function postRegister(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required|email|unique:users','password'=>'required|min:6'
        ]);
        $data=$request->all();
        $check=$this->create($data);
        
        return redirect()->back()->with('message','Data berhasil di input');
    }
    public function create(array $data){
        return User::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'password'=> Hash::make($data['password'])
        ]);
    }
}
