<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/administrator/login','AdministratorAuth@showpagelogin')->name('administrator.login');
Route::post('administrator-login','AdministratorAuth@postLoginAdmin')->name('administrator.postlogin');
Route::get('administrator/register','AdministratorAuth@showpageregister')->name('administrator.register');
Route::post('administrator-register','AdministratorAuth@postRegister')->name('administrator.postregister');